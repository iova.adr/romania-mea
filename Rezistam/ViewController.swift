//
//  ViewController.swift
//  Rezistam
//
//  Created by IOVA ADRIAN on 07/02/2017.
//  Copyright © 2017 IOVA ADRIAN. All rights reserved.
//

import UIKit
import Firebase
import SwiftyButton

class ViewController: UIViewController {

    @IBOutlet weak var blueButton: FlatButton!
    @IBOutlet weak var yellowButton: FlatButton!
    @IBOutlet weak var redButton: FlatButton!
    @IBOutlet weak var cuVictorieiLabel: UILabel!
    @IBOutlet weak var inVictorieiLabel: UILabel!
    @IBOutlet weak var nuSustinLabel: UILabel!
    
    var ref: FIRDatabaseReference!
    var uniqueId: String!
    var allPeople = [People]()
    var inVictoriei = 0
    var cuVictoriei = 0
    var nuSustin = 0
    var people = People()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = FIRDatabase.database().reference()
        
        FIRAuth.auth()?.signInAnonymously() { (user, error) in
            
            UserData.sharedInstance.id = user!.uid
            self.uniqueId = UserData.sharedInstance.id
            
            
            self.ref.child("people").child(self.uniqueId).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let snap = snapshot.value  as? Dictionary<String, Bool> {
                    if let cu = snap["inVictoriei"] {
                        if cu {
                            self.people.inVictoriei = true
                        } else {
                            self.people.inVictoriei = false
                        }
                    }
                    if let cu = snap["cuVictoriei"] {
                        if cu {
                            self.people.cuVictoriei = true
                        } else {
                            self.people.cuVictoriei = false
                        }
                    }
                    if let cu = snap["nuSustin"] {
                        if cu {
                            self.people.nuSustin = true
                        } else {
                            self.people.nuSustin = false
                        }
                    }
                }
                
                if self.people.inVictoriei {
                    self.redButton.color = hexStringToUIColor(hex: "2FC407")
                } else {
                    self.redButton.color = .lightGray
                }
                
                if self.people.cuVictoriei {
                    self.yellowButton.color = hexStringToUIColor(hex: "2FC407")
                } else {
                    self.yellowButton.color = .lightGray
                }
                
                if self.people.nuSustin {
                    self.blueButton.color = hexStringToUIColor(hex: "2FC407")
                } else {
                    self.blueButton.color = .lightGray
                }
                
            })
            
            
            self.ref.child("people").observe(.value, with: { (snapshot) in
                //print(snapshot.value)
                
                self.inVictoriei = 0
                self.cuVictoriei = 0
                self.nuSustin = 0
                
                if let snapshots = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    for snap in snapshots {
                        if let postDict = snap.value as? Dictionary<String, Bool> {
                            if let status = postDict["inVictoriei"] {
                                if status == true {
                                    self.inVictoriei += 1
                                }
                            }
                            
                            if let status = postDict["cuVictoriei"] {
                                if status == true {
                                    self.cuVictoriei += 1
                                }
                            }
                            
                            if let status = postDict["nuSustin"] {
                                if status == true {
                                    self.nuSustin += 1
                                }
                            }
                        }
                    }
                }
                self.inVictorieiLabel.text = String(self.inVictoriei)
                self.cuVictorieiLabel.text = String(self.cuVictoriei)
                self.nuSustinLabel.text = String(self.nuSustin)
            })
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doRed(_ sender: Any) {
        if self.people.inVictoriei {
            self.ref.child("people").child(uniqueId).child("inVictoriei").setValue(false)
            self.people.inVictoriei = false
            self.redButton.color = .lightGray
        } else {
            self.ref.child("people").child(uniqueId).child("inVictoriei").setValue(true)
            self.ref.child("people").child(uniqueId).child("cuVictoriei").setValue(false)
            self.ref.child("people").child(uniqueId).child("nuSustin").setValue(false)
            self.people.inVictoriei = true
            self.people.cuVictoriei = false
            self.people.nuSustin = false
            self.redButton.color = hexStringToUIColor(hex: "2FC407")
        }
    }
    
    @IBAction func doYellow(_ sender: Any) {
        if self.people.cuVictoriei {
            self.ref.child("people").child(uniqueId).child("cuVictoriei").setValue(false)
            self.people.cuVictoriei = false
            self.yellowButton.color = .lightGray
        } else {
            self.ref.child("people").child(uniqueId).child("inVictoriei").setValue(false)
            self.ref.child("people").child(uniqueId).child("cuVictoriei").setValue(true)
            self.ref.child("people").child(uniqueId).child("nuSustin").setValue(false)
            self.people.cuVictoriei = true
            self.yellowButton.color = hexStringToUIColor(hex: "2FC407")
        }
    }
    
    @IBAction func doBlue(_ sender: Any) {
        if self.people.nuSustin {
            self.ref.child("people").child(uniqueId).child("nuSustin").setValue(false)
            self.people.nuSustin = false
            self.blueButton.color = .lightGray
        } else {
            self.ref.child("people").child(uniqueId).child("inVictoriei").setValue(false)
            self.ref.child("people").child(uniqueId).child("cuVictoriei").setValue(false)
            self.ref.child("people").child(uniqueId).child("nuSustin").setValue(true)
            self.people.nuSustin = true
            self.blueButton.color = hexStringToUIColor(hex: "2FC407")
        }
    }
}

class People : NSObject {
    var inVictoriei = false
    var cuVictoriei = false
    var nuSustin = false
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.characters.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}


